import java.util.List;
import java.util.Set;
/*
 * Creates a board, and checks that board for wins
 */
public class Board {
	private List<Checkers> AICheckersList = new List<Checkers>();
	private Set<Checkers> playerCheckersList = new Set<Checkers>();
	private Checkers [] [] Board;
	/* 
	 * sets up the board, list, and set of checkers, and puts the two lists on the board
	 */
	public Board(){
		Board = new Checkers [8] [8];
		Checkers A1 = new Checkers(false,1,0);
		Board [1] [0] = A1;
		Checkers A2 = new Checkers(false,3,0);
		Board [3] [0] = A2;
		Checkers A3 = new Checkers(false,5,0);
		Board [5] [0] = A3;
		Checkers A4 = new Checkers(false,7,0);
		Board [7] [0] = A4;
		Checkers A5 = new Checkers(false,0,1);
		Board [0] [1] = A5;
		Checkers A6 = new Checkers(false,2,1);
		Board [2] [1] = A6;
		Checkers A7 = new Checkers(false,4,1);
		Board [4] [1] = A7;
		Checkers A8 = new Checkers(false,6,1);
		Board [6] [1] = A8;
		Checkers A9 = new Checkers(false,1,2);
		Board [1] [2] = A9;
		Checkers A10 = new Checkers(false,3,2);
		Board [3] [2] = A10;
		Checkers A11 = new Checkers(false,5,2);
		Board [5] [2] = A11;
		Checkers A12 = new Checkers(false,7,2);
		Board [7] [2] = A12;
		Checkers P1 = new Checkers(false,0,5);
		Board [0] [5] = P1;
		Checkers P2 = new Checkers(false,2,5);
		Board [2] [5] = P2;
		Checkers P3 = new Checkers(false,4,5);
		Board [4] [5] = P3;
		Checkers P4 = new Checkers(false,6,5);
		Board [6] [5] = P4;	
		Checkers P5 = new Checkers(false,1,6);
		Board [1] [6] = P5;
		Checkers P6 = new Checkers(false,3,6);
		Board [3] [6] = P6;
		Checkers P7 = new Checkers(false,5,6);
		Board [5] [6] = P7;
		Checkers P8 = new Checkers(false,7,6);
		Board [7] [6] = P8;
		Checkers P9 = new Checkers(false,0,7);
		Board [1] [0] = P9;
		Checkers P10 = new Checkers(false,2,7);
		Board [1] [0] = P10;
		Checkers P11 = new Checkers(false,4,7);
		Board [1] [0] = P11;
		Checkers P12 = new Checkers(false,6,7);
		Board [1] [0] = P12;
		AICheckersList.add(A1);
		AICheckersList.add(A2);
		AICheckersList.add(A3);
		AICheckersList.add(A4);
		AICheckersList.add(A5);
		AICheckersList.add(A6);
		AICheckersList.add(A7);
		AICheckersList.add(A8);
		AICheckersList.add(A9);
		AICheckersList.add(A10);
		AICheckersList.add(A11);
		AICheckersList.add(A12);
		playerCheckersList.add(P1);
		playerCheckersList.add(P2);
		playerCheckersList.add(P3);
		playerCheckersList.add(P4);
		playerCheckersList.add(P5);
		playerCheckersList.add(P6);
		playerCheckersList.add(P7);
		playerCheckersList.add(P8);
		playerCheckersList.add(P9);
		playerCheckersList.add(P10);
		playerCheckersList.add(P11);
		playerCheckersList.add(P12);
		
	}
	/*
	 * checks if the list or set is empty, and awards a winner
	 */
	public void GameWinner(){
		
	}
	/*
	 * displays the current board
	 */
	public void DisplayBoard(){
		
	}
}
